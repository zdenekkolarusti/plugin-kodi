"""
    Stream cinema service.
"""
import traceback

import xbmc

from resources.lib.auth.auth_provider import AuthProvider
from resources.lib.defaults import Defaults
from resources.lib.kodilogging import service_logger
from resources.lib.kodilogging import setup_root_logger
from resources.lib.players.stream_cinema_player import StreamCinemaPlayer
from resources.lib.services.download_service import DownloadService
from resources.lib.services.library_service import LibraryService
from resources.lib.services.monitor_service import MonitorService
from resources.lib.services.player_service import PlayerService
from resources.lib.services.routing_service import RoutingService
from resources.lib.services.settings_service import SettingsService
from resources.lib.services.trakt_service import TraktService
from resources.lib.services.update_service import UpdateService
from resources.lib.stream_cinema import StreamCinema
from resources.lib.utils.serviceutils import first_run


def try_except(fn):
    try:
        fn()
    except Exception as e:
        service_logger.error(traceback.format_exc())
        service_logger.error(e)


setup_root_logger()
threads = []
service_logger.debug('Main service started')

settings_service = SettingsService()
provider = Defaults.provider()
api = Defaults.api()

monitor_service = MonitorService(True)
player_service = PlayerService(Defaults.api(), monitor_service=monitor_service)
stream_cinema_player = StreamCinemaPlayer(service=player_service)
stream_cinema = StreamCinema(provider, api, player_service, threads)

auth_provider = AuthProvider(Defaults.provider())

first_run(auth_provider)

update_service = UpdateService(True, auth_provider)
update_service.check()
routing_service = RoutingService(update_service, stream_cinema)

download_service = DownloadService(Defaults.api())
download_service.process_queue()

trakt_service = TraktService()
try_except(trakt_service.sync)
library_service = LibraryService()
try_except(library_service.sync)

xbmc_monitor = xbmc.Monitor()
service_logger.debug('All subservices started!')

timer_services = [update_service, trakt_service, library_service, monitor_service]
thread_services = [routing_service, download_service]

while not xbmc_monitor.abortRequested():
    xbmc_monitor.waitForAbort()

for service in thread_services:
    service.stop()

for service in timer_services:
    service.stop()

# TODO: Fix warnings about some classes left in a memory:
# https://forum.kodi.tv/showthread.php?tid=307508&pid=2531105#pid2531105
service_logger.debug('Main service stopped')
