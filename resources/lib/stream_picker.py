import os

from resources.lib.api.api import API
from resources.lib.compatibility import HTTPError, encode_utf, decode_utf
from resources.lib.const import SETTINGS, HTTP_METHOD, LANG, SERVICE, ACTION, DOWNLOAD_SERVICE_EVENT, DOWNLOAD_STATUS, \
    trakt_type_map, MEDIA_TYPE
from resources.lib.defaults import Defaults
from resources.lib.gui import InfoDialog, MediaItem
from resources.lib.gui.renderers.dialog_renderer import DialogRenderer
from resources.lib.gui.renderers.media_info_renderer import MediaInfoRenderer
from resources.lib.kodilogging import logger
from resources.lib.routing.router import router
from resources.lib.storage.settings import settings
from resources.lib.storage.sqlite import DB
from resources.lib.utils import streams
from resources.lib.utils.addtolib import normalize_name
from resources.lib.utils.kodiutils import get_string, refresh, convert_bitrate, swap_keys_with_values
from resources.lib.utils.url import Url
from resources.lib.wrappers.http import Http


class StreamPicker:
    def __init__(self, plugin_core):
        self._plugin_core = plugin_core

        self.permission_map_playback = {
            True: self.process_stream,
            False: self.login_process_stream
        }

        self.stream_action_map = {
            ACTION.PLAY: self.play_stream,
            ACTION.DOWNLOAD: self.download_stream,
        }

    def stream_guard(self, *args):
        self.permission_map_playback[settings[SETTINGS.PROVIDER_LOGGED_IN]](*args)

    def process_stream(self, handle, stream, media_id, root_parent_id, action):
        if self._plugin_core.auth.check_account():
            self.stream_action_map[action](handle, stream, media_id, root_parent_id)
        else:
            router.set_resolved_url(handle)

    def login_process_stream(self, handle, stream, media_id, root_parent_id, action):
        if self._plugin_core.auth.set_provider_credentials():
            self.process_stream(handle, stream, media_id, root_parent_id, action)
        else:
            router.set_resolved_url(handle)

    def download_stream(self, handle, stream, media_id, root_parent_id):
        stream_url = self.get_stream_url(handle, stream)
        if not stream_url:
            return
        a = Url.urlparse(stream_url)
        filename = os.path.basename(a.path)
        original_title = self.title_for_download(media_id)
        if original_title != '':
            filename = original_title + '_' + filename
        if not (filename.endswith('.mkv') or filename.endswith('avi') or filename.endswith('mp4')):
            filename += '.mkv'
        download_folder = settings[SETTINGS.DOWNLOADS_FOLDER]
        if download_folder == '':
            InfoDialog(get_string(LANG.DL_DIR_NOT_SET), sound=True).notify()
            return
        dl_item = DB.DOWNLOAD.get_by_filename(download_folder, filename)
        if dl_item:
            status = dl_item[4]
            if status == DOWNLOAD_STATUS.CANCELLED or status == DOWNLOAD_STATUS.COMPLETED:
                DB.DOWNLOAD.set_status(dl_item[0], DOWNLOAD_STATUS.QUEUED)
                self.download(dl_item[0])
                refresh()
                return
            elif status == DOWNLOAD_STATUS.QUEUED or status == DOWNLOAD_STATUS.DOWNLOADING:
                InfoDialog(get_string(LANG.ALREADY_DOWNLOADING)).notify()
                return
        InfoDialog(get_string(LANG.STARTING_DOWNLOAD).format(filename=original_title)).notify()
        self.download_new(stream_url, download_folder, filename, media_id)

    def download_new(self, url, path, name, media_id):
        dl_id = DB.DOWNLOAD.add(url, path, name, media_id)
        self.download(dl_id)

    def download(self, dl_id):
        self._plugin_core.send_service_message(SERVICE.DOWNLOAD_SERVICE, DOWNLOAD_SERVICE_EVENT.ITEM_ADDED)

    def get_streams(self, url):
        response = self._plugin_core.api_request(HTTP_METHOD.GET, url)
        if len(response) == 0:
            msg = get_string(LANG.MISSING_STREAM_TEXT) + ' ' + get_string(LANG.TELL_US_ABOUT_IT)
            DialogRenderer.ok(get_string(LANG.MISSING_STREAM_TITLE), msg)
            return
        return response

    def get_stream_url(self, handle, stream):
        stream_url = self._plugin_core.provider.get_link_for_file_with_id(stream.get('ident'))
        if not stream_url:
            self._plugin_core.api.report_stream(stream.get('_id'))
            msg = get_string(LANG.STREAM_NOT_AVAILABLE) + ' ' + get_string(LANG.STREAM_FLAGGED_UNAVAILABLE)
            DialogRenderer.ok(get_string(LANG.MISSING_STREAM_TITLE), msg)
            router.set_resolved_url(handle)
        else:
            r = Http.head(stream_url)
            if '?error=' in stream_url or '?error=' in r.url:
                DialogRenderer.ok(get_string(LANG.PROVIDER_DIDNT_RETURN_VALID_LINK),
                                  get_string(LANG.SOMEONE_ELSE_IS_WATCHING))
                router.set_resolved_url(handle)
                return
        return stream_url

    def play_stream(self, handle, stream, media_id, root_parent_id):
        logger.debug('Trying to play stream.')
        stream_url = self.get_stream_url(handle, stream)
        if stream_url:
            media = self._plugin_core.api_request(HTTP_METHOD.GET, API.URL.media_detail(media_id))
            self.play(handle, media, stream, stream_url, root_parent_id)

    def play(self, handle, media, stream, stream_url, root_parent_id):
        langs = MediaInfoRenderer.get_language_priority()
        labels, art = MediaInfoRenderer.merge_info_labels(media, langs)
        media_type = labels.get('mediatype')
        ext_subs = False
        trakt_id = media.get('services').get('trakt') if 'trakt' in media.get('services') else {}
        if trakt_id:
            type_map = swap_keys_with_values(trakt_type_map)
            trakt_id = {type_map[media_type]: {'ids': {'trakt': trakt_id}}}
        title = labels.get('title')
        
        if not self._plugin_core.has_preferred_subtitles(stream):
            logger.debug('Subtitles are missing. Trying to find some.')
            subtitles_string = MediaInfoRenderer.TITLE.subtitles_string(labels)
            ext_subs = self._plugin_core.get_subtitles(media,subtitles_string)
            
        url, item, directory = MediaItem(title, url=stream_url, cast=media.get('cast'), art=art,
                                         info_labels=labels).build()                                        
        if ext_subs: item.setSubtitles([ext_subs])

        logger.debug('Stream URL found. Playing %s' % url)
        self._plugin_core.player.play(handle, item, url, media_id=root_parent_id, trakt_id=trakt_id)

    @staticmethod
    def title_for_download(media_id):
        res = ''
        try:
            res = Defaults.api().request(HTTP_METHOD.GET, API.URL.media_detail(media_id))
            
        except HTTPError as err:
            logger.error(err)
        if res != '':
            media_detail = res.json()
            langs = MediaInfoRenderer.get_language_priority()
            labels, art = MediaInfoRenderer.merge_info_labels(media_detail, langs)
            download_title = MediaInfoRenderer.TITLE.subtitles_string(labels)       
        return normalize_name(decode_utf(download_title))

    @staticmethod
    def choose_stream(stream_list):
        return DialogRenderer.choose_video_stream(stream_list)

    def select_stream(self, stream_list, force):
        selected_stream = None
        if settings[SETTINGS.STREAM_AUTOSELECT] and force == '0':
            selected_stream = self._choose_stream_auto(stream_list)

        if selected_stream is None:
            selected_stream = self.choose_stream(stream_list)
        return selected_stream

    def get_and_select_stream(self, url, force):
        stream_list = self.get_streams(url)
        if stream_list:
            return self.select_stream(StreamPicker.filter_streams(stream_list), force)

    @staticmethod
    def is_hdr(video):
        return video.get('hdr')

    @staticmethod
    def is_3d(video):
        return video.get('3d')

    @staticmethod
    def filter_streams(stream_list):
        filtered_list = []
        for stream in stream_list:
            video = stream.get('video', [])
            if len(video) == 0:
                continue

            video = video[0]
            if not settings[SETTINGS.SHOW_HDR_STREAMS] and StreamPicker.is_hdr(video):
                continue

            if not settings[SETTINGS.SHOW_3D_STREAMS] and StreamPicker.is_3d(video):
                continue

            if video.get('duration') and stream.get('size'):
                bit_rate = stream.get('size') / video.get('duration') * 8
                max_bit_rate = settings[SETTINGS.STREAM_MAX_BITRATE_READABLE]
                if settings[SETTINGS.MAX_BITRATE_FILTER]:
                    bit_rate = convert_bitrate(bit_rate, False)
                    if 0 < max_bit_rate < bit_rate < 200:
                        continue
            filtered_list.append(stream)
        return filtered_list

    @staticmethod
    def _choose_stream_auto(response):
        preferred_quality = settings[SETTINGS.STREAM_AUTOSELECT_PREFERRED_QUALITY]
        preferred_languages = [settings[SETTINGS.PREFERRED_LANGUAGE],
                               settings[SETTINGS.FALLBACK_LANGUAGE]]
        max_bitrate = settings[SETTINGS.STREAM_AUTOSELECT_MAX_BITRATE] if settings[
            SETTINGS.STREAM_AUTOSELECT_LIMIT_BITRATE] else 0
        avoid_vcodecs = (settings[SETTINGS.STREAM_AUTOSELECT_AVOID_VIDEO_CODECS] or '').split(',')
        preferred_channels = settings[SETTINGS.STREAM_AUTOSELECT_PREFERRED_CHANNELS] if settings[
            SETTINGS.STREAM_AUTOSELECT_PREFERRED_CHANNELS_ENABLED] else None
        return streams.select(response,
                              audio=preferred_languages,
                              max_bitrate=max_bitrate,
                              vquality=preferred_quality,
                              channels=preferred_channels,
                              codec_blacklist=avoid_vcodecs)
