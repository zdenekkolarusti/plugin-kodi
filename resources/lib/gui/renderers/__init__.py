from resources.lib.routing.router import router


class Renderer(object):
    @property
    def handle(self):
        return router.handle

    @property
    def router(self):
        return router

    @staticmethod
    def url_for(*args, **kwargs):
        return router.url_for(*args, **kwargs)
