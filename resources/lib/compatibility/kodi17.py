# keep it here for default imports
from resources.lib.compatibility.default import *
from xmlrpclib import ServerProxy, Transport
from urllib2 import HTTPError


def get_setting_as_bool(addon, setting):
    from resources.lib.compatibility.kodi16 import get_setting_as_bool_kodi16
    return get_setting_as_bool_kodi16(addon, setting)
