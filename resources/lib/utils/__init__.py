def first(col):
    return next(iter(col or []), None)